package com.cubuanic.stepik.java.collections;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

@RunWith(Enclosed.class)
public class Step1_2_7Test {
    public static class Step1_2_7SingleTests {
        @Test(timeout = 5000)
        public void instantiate() {
            checkClassInstantiation();
        }
    }

    @RunWith(Parameterized.class)
    public static class CallMethodByWeightParameterizedTests {
        private String pattern;
        private String output;

        public CallMethodByWeightParameterizedTests(String pattern, String output) {
            this.pattern = pattern;
            this.output = output;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                {"([][])", "true"},
                {"[()]{}{[()()]()}", "true"},

                {"()[]}", "false"},
                {"()[]]", "false"},
                {"()[])", "false"},

                {"({}]", "false"},
                {"({]]", "false"},
                {"({)]", "false"},

                {"([}]", "false"},
                {"([]]", "false"},
                {"([)]", "false"},

                {"()[", "false"},
                {"()a", "false"},
            });
        }

        @Test(timeout = 5000)
        public void test() throws Throwable {
            runInputOutputTest(pattern, output);
        }
    }
}