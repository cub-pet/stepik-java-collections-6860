package com.cubuanic.stepik.java.collections;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

import org.junit.Test;

public class Step1_4_7Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testPairsInRange() throws Throwable {
        runInputOutputTest("2 4\n" +
                "5\n" +
                "1 aa\n" +
                "5 ee\n" +
                "2 bb\n" +
                "4 dd\n" +
                "3 cc\n",
            "2 bb\n" +
                "3 cc\n" +
                "4 dd\n"
        );
    }
}