package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import java.util.*;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Step1_3_4Test {
    @Test(timeout = 5000)
    public void testA() {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(1);
        set2.add(2);
        set2.add(3);

        assertTrue(set1.equals(set2));
    }

    @Test(timeout = 5000)
    public void testB() {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(3);
        set2.add(2);
        set2.add(1);

        assertTrue(set1.equals(set2));
    }

    @Test(timeout = 5000)
    public void testC() {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new LinkedHashSet<>();
        set2.add(1);
        set2.add(3);
        set2.add(2);

        assertTrue(set1.equals(set2));
    }

    @Test(timeout = 5000)
    public void testD() {
        Set<Integer> set1 = new LinkedHashSet<>();
        set1.add(1);
        set1.add(3);
        set1.add(2);

        Set<Integer> set2 = new LinkedHashSet<>();
        set2.add(1);
        set2.add(3);

        assertFalse(set1.equals(set2));
    }

    @Test(timeout = 5000)
    public void testE() {
        SortedSet<Integer> set1 = new TreeSet<>();
        set1.add(1);
        set1.add(3);

        Set<Integer> set2 = new LinkedHashSet<>();
        set2.add(3);
        set2.add(1);

        assertTrue(set1.equals(set2));
    }
}