package com.cubuanic.stepik.java.collections;


import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_4_6bTest {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testSubstitutionCipher() throws Throwable {
        runInputOutputTest("abcd\n" +
                "*d%#\n" +
                "abacabadaba\n" +
                "#*%*d*%",
            "*d*%*d*#*d*\n" +
                "dacabac"
        );
    }
}