package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_1_10Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() throws Throwable {
        runInputOutputTest("", "[]");
    }

    @Test(timeout = 5000)
    public void test2() throws Throwable {
        runInputOutputTest("1", "[1]");
    }

    @Test(timeout = 5000)
    public void test3() throws Throwable {
        runInputOutputTest("1 2", "[1, 2]");
    }

    @Test(timeout = 5000)
    public void test4() throws Throwable {
        runInputOutputTest("Google Oracle JetBrains", "[Google, Oracle, JetBrains]");
    }
}
