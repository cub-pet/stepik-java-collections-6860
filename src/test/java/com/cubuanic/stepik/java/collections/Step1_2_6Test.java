package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_2_6Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() throws Throwable {
        runInputOutputTest("4\n" +
                        "1\n" +
                        "2\n" +
                        "3\n" +
                        "4\n",
                "4\n" +
                        "2\n" +
                        "1\n" +
                        "3\n");
    }

    @Test(timeout = 5000)
    public void test2() throws Throwable {
        runInputOutputTest("3\n" +
                        "1\n" +
                        "2\n" +
                        "3\n",
                "2\n" +
                        "1\n" +
                        "3\n");
    }

    @Test(timeout = 5000)
    public void test3() throws Throwable {
        runInputOutputTest("4\n" +
                        "4\n" +
                        "3\n" +
                        "2\n" +
                        "1\n",
                "2\n" +
                        "4\n" +
                        "3\n" +
                        "1\n");
    }
}