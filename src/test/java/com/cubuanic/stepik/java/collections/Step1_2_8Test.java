package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_2_8Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() throws Throwable {
        runInputOutputTest("6\n" +
                        "1 1\n" +
                        "2 1\n" +
                        "3 1\n" +
                        "4 3\n" +
                        "5 1\n" +
                        "6 1",
                "1 3 5 6\n" +
                        "2 4");
    }
}