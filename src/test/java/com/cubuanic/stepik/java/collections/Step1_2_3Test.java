package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_2_3Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() throws Throwable {
        runInputOutputTest("", "[3, 4, 5]");
    }
}