package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_2_4Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }
}