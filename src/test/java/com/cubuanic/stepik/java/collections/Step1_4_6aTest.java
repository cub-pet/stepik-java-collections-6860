package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static com.cubuanic.TestUtils.*;
import static org.junit.Assert.assertTrue;

public class Step1_4_6aTest {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testSubstitutionCipher() throws Throwable {
        runInputOutputTest("abcd\n" +
                "*d%#\n" +
                "abacabadaba\n" +
                "#*%*d*%",
            "*d*%*d*#*d*\n" +
                "dacabac"
        );
    }
}