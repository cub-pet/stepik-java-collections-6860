package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_4_4Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testWarAndPeace() throws Throwable {
        runInputOutputTest("a aa abC aa ac abc bcd a",
            "a 2\n" +
                "aa 2\n" +
                "abc 2\n" +
                "ac 1\n" +
                "bcd 1"
        );
    }
}