package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runOutputTest;

public class Step1_3_3Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testSetOutput() throws Throwable {
        runOutputTest("Mr.Green\n" +
            "Mr.Red\n" +
            "Mr.Yellow");
    }
}