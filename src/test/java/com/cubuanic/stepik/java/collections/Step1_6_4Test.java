package com.cubuanic.stepik.java.collections;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

import org.junit.Test;

public class Step1_6_4Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testPairsInRange() throws Throwable {
        runInputOutputTest("3 3\n" +
                "1 1 1\n" +
                "2 2 2\n" +
                "3 3 3\n" +
                "1",
            "3 3 3\n" +
                "1 1 1\n" +
                "2 2 2"
        );
    }
}