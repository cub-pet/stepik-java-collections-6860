package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_3_7Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testFilterInput1() throws Throwable {
        runInputOutputTest("6\n" +
                "postgres\n" +
                "sqlite\n" +
                "oracle\n" +
                "mongodb\n" +
                "postgres\n" +
                "mssql\n" +
                "mssql\n" +
                "mssql\n" +
                "postgres",
            "mongodb\n" +
                "mssql\n" +
                "oracle\n" +
                "postgres\n" +
                "sqlite"
        );
    }

    @Test(timeout = 5000)
    public void testFilterInput2() throws Throwable {
        runInputOutputTest("6\n" +
                "sqlite\n" +
                "oracle\n" +
                "mongodb\n" +
                "mssql\n" +
                "postgres",
            "mongodb\n" +
                "mssql\n" +
                "oracle\n" +
                "postgres\n" +
                "sqlite"
        );
    }
}