package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;

public class Step1_1_9Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }
}