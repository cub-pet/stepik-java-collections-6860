package com.cubuanic.stepik.java.collections;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

import org.junit.Test;

public class Step1_6_2Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testPairsInRange() throws Throwable {
        runInputOutputTest("1 2 3 4 5 6\n" +
                "2\n" +
                "0 1\n" +
                "3 4",
            "2 1 3 5 4 6\n"
        );
    }
}