package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import static com.cubuanic.TestUtils.checkClassInstantiation;
import static com.cubuanic.TestUtils.runInputOutputTest;

public class Step1_1_8Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void test1() throws Throwable {
        runInputOutputTest("", "");
    }

    @Test(timeout = 5000)
    public void test2() throws Throwable {
        runInputOutputTest("1", "");
    }

    @Test(timeout = 5000)
    public void test3() throws Throwable {
        runInputOutputTest("1 2", "2");
    }

    @Test(timeout = 5000)
    public void test4() throws Throwable {
        runInputOutputTest("1 2 3 4 5 6 7", "6 4 2");
    }

    @Test(timeout = 5000)
    public void test5() throws Throwable {
        runInputOutputTest("1 2 3 4 5 6 7 8", "8 6 4 2");
    }
}
