package com.cubuanic.stepik.java.collections;

import org.junit.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static com.cubuanic.TestUtils.catchOutputFromInput;
import static com.cubuanic.TestUtils.checkClassInstantiation;
import static org.junit.Assert.assertTrue;

public class Step1_4_5Test {
    @Test(timeout = 5000)
    public void instantiate() {
        checkClassInstantiation();
    }

    @Test(timeout = 5000)
    public void testSpellChecker() throws Throwable {
        String output = catchOutputFromInput("3\n" +
            "a\n" +
            "bb\n" +
            "cCc\n" +
            "2\n" +
            "a bb aab aba ccc\n" +
            "c bb aaa");
        Set<String> outputSet = stringToSet(output);
        Set<String> expectedSet = stringToSet("aaa\n" +
            "aab\n" +
            "c\n" +
            "aba"
        );
        assertTrue(expectedSet.equals(outputSet));
    }

    private Set<String> stringToSet(String str) {
        return Arrays.stream(str.split("[ \n]")).collect(Collectors.toSet());
    }
}