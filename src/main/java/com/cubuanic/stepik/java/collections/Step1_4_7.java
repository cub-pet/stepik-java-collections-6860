package com.cubuanic.stepik.java.collections;

import java.util.Scanner;
import java.util.TreeMap;

public class Step1_4_7 {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int from = scanner.nextInt();
    int to = scanner.nextInt();
    int count = scanner.nextInt();

    TreeMap<Integer, String> pairs = new TreeMap<>();

    while (count-- > 0) {
        Integer key = scanner.nextInt();
        String val = scanner.next();
        pairs.put(key, val);
    }

    pairs
        .subMap(from, true, to, true)
        .entrySet()
        .stream()
        .map(e -> e.getKey() + " " + e.getValue())
        .forEach(System.out::println);
    }
}
