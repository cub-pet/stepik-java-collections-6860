package com.cubuanic.stepik.java.collections;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Step1_3_3 {
    public static void main(String[] args) {
        Set<String> nameSet = new TreeSet<>(Arrays.asList("Mr.Green", "Mr.Yellow", "Mr.Red"));
        nameSet.forEach(System.out::println);
    }
}
