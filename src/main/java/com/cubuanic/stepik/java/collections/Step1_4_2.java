package com.cubuanic.stepik.java.collections;

import java.util.TreeMap;

public class Step1_4_2 {
    public static void main(String[] args) {
        TreeMap<String, Integer> map = new TreeMap();

        map.put("Gamma", 3);
        map.put("Alpha", 1);
        map.put("Omega", 24);

        System.out.println(map);
    }
}
