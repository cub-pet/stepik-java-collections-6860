package com.cubuanic.stepik.java.collections;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Step1_6_2 {
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);

        String elementsLine = inputScanner.nextLine();
        Scanner elementsScanner = new Scanner(elementsLine);
        List<Integer> elements = new ArrayList<>();
        while (elementsScanner.hasNextInt()) {
            elements.add(elementsScanner.nextInt());
        }

        int count = inputScanner.nextInt();
        while (count-- > 0) {
            int from = inputScanner.nextInt();
            int to = inputScanner.nextInt();
            Collections.swap(elements, from, to);
        }

        System.out.println(elements
            .stream()
            .map(Object::toString)
            .collect(joining(" "))
        );
    }
}
