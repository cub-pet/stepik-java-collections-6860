package com.cubuanic.stepik.java.collections;

import java.util.Map;
import java.util.TreeMap;

public class Step1_4_3 {
    public static void main(String[] args) {
        Map<String, Integer> map = new TreeMap<>();
        map.put("Gamma",  3);
        map.put("Omega", 24);
        map.put("Alpha",  1);

        map.forEach((k,v) -> System.out.println(k + "=" + v));
    }
}
