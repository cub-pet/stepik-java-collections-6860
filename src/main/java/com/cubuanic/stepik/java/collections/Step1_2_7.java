package com.cubuanic.stepik.java.collections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;

public class Step1_2_7 {
    /**
     * Given a string consisting of brackets, write a program to examine whether
     * the pairs and the orders of "{", "}", "(", ")", "[", "]" are correct (balanced).
     * For example, the program should print true for the string [()]{}{[()()]()} and false for ()[]}.
     *
     * The classic algorithm for solving this problem relies on using a stack.
     * - create an instance of a stack
     * - traverse the input string
     * - if the current character is a starting bracket "(" or "{" or "{" then push it to the stack
     * - if the current is a closing bracket ")" or "}" or "]" then remove (pop) the top element from the stack
     * -- if the popped bracket is the matching starting bracket then fine else parenthesis are not balanced
     * After completing traversal, if there are some starting brackets left in the stack,
     * then the parenthesis are not balanced.
     *
     * Sample Input: ([][])
     * Sample Output: true
     */
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Deque<Character> stack = new ArrayDeque<>();
        final String input = reader.readLine();
        for (int i = 0; i < input.length(); i++) {
            char inputChar = input.charAt(i);
            if (inputChar == '[' || inputChar == '(' || inputChar == '{') {
                stack.push(inputChar);
            } else if (inputChar == ']' || inputChar == ')' || inputChar == '}') {
                if (stack.isEmpty()) {
                    System.out.println("false");
                    return;
                }
                char bracket = stack.pop();
                if (bracket != '[' && inputChar == ']'
                    || bracket != '(' && inputChar == ')'
                    || bracket != '{' && inputChar == '}'
                ) {
                    System.out.println("false");
                    return;
                }
            } else {
                System.out.println("false");
                return;
            }
        }
        if (!stack.isEmpty()) {
            System.out.println("false");
            return;
        }
        System.out.println("true");
    }
}
