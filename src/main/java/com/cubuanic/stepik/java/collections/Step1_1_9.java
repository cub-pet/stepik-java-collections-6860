package com.cubuanic.stepik.java.collections;

import com.cubuanic.Generated;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Step1_1_9 {

    // avoid by jacoco
    @Generated(message = "manual")
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        List<Integer> list2 = new LinkedList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);

        List<Integer> list3 = new ArrayList<>();
        list3.add(3);
        list3.add(1);
        list3.add(2);

        System.out.println(list1.equals(list2) ? "true" : "false");
        System.out.println(list1.equals(list1) ? "true" : "false");
        System.out.println(list1.equals(list3) ? "true" : "false");
        System.out.println(list3.equals(list2) ? "true" : "false");
    }
}
