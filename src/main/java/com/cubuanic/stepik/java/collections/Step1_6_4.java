package com.cubuanic.stepik.java.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Step1_6_4 {
    public static void main(String[] args) {
    Scanner inputScanner = new Scanner(System.in);
    Scanner elementsScanner = new Scanner(inputScanner.nextLine());

    int rows = elementsScanner.nextInt();
    int cols = elementsScanner.nextInt();

    List<List<String>> elements = new ArrayList<>();
    while (rows-- > 0) {
        Scanner itemsScanner = new Scanner(inputScanner.nextLine());
        List<String> items = new ArrayList<>();
        while (itemsScanner.hasNext()) {
            items.add(itemsScanner.next());
        }
        elements.add(items);
    }

    int distance = inputScanner.nextInt();
    Collections.rotate(elements, distance);

    elements
        .stream()
        .map(list -> String.join(" ", list))
        .forEach(System.out::println);
    }
}
