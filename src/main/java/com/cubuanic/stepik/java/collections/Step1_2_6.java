package com.cubuanic.stepik.java.collections;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class Step1_2_6 {
    /**
     * Write a program that reads numbers and stores them to a deque.
     * An even number should be added as the first element, an odd number - as the last.
     * After, the program must output all elements from the first to the last.
     *
     * The first string contains the number of elements.
     * Each line followed the first one contains an element.
     *
     * Sample Input:
     * 4
     * 1
     * 2
     * 3
     * 4
     * Sample Output:
     * 4
     * 2
     * 1
     * 3
     */
    public static void main(String[] args) {
        Deque<Integer> stack = new ArrayDeque<>();
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        while (count-- > 0) {
            int data = scanner.nextInt();
            if (data % 2 == 0) {
                stack.push(data);
            } else {
                stack.add(data);
            }
        }

        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
    }
}
