package com.cubuanic.stepik.java.collections;

import com.cubuanic.Generated;

import java.util.ArrayDeque;
import java.util.Deque;

@SuppressWarnings("PMD.UnusedLocalVariable")
public class Step1_2_4 {
    /**
     * What does this code print?
     * <p>
     * GermanyFranceUK
     * GermanyFranceNorway      <--
     * UKGermanyFranceNorway
     * GermanyFranceNorwayUK
     */

    // avoid by jacoco
    @Generated(message = "manual")
    public static void main(String[] args) {
        Deque<String> states = new ArrayDeque<String>();

        states.add("Germany");
        states.add("France");
        states.push("UK");
        states.offerLast("Norway");

        String sFirst = states.pop();
        String s = states.peek();
        String sLast = states.peekLast();
        states.offer(sFirst);
        String s1 = states.pollLast();

        while (states.peek() != null) {
            System.out.print(states.pop());
        }
    }
}
