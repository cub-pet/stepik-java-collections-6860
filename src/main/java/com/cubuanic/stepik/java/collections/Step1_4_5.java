package com.cubuanic.stepik.java.collections;

import java.util.*;

public class Step1_4_5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<String> vocabulary = new HashSet<>();

        int count = Integer.parseInt(scanner.nextLine());
        while (count --> 0) {
            vocabulary.add(scanner.nextLine().toLowerCase());
        }

        count = scanner.nextInt();
        Set<String> errors = new HashSet<>();
        while (scanner.hasNext()) {
            final String word = scanner.next().toLowerCase();
            if (!vocabulary.contains(word)) {
                errors.add(word);
            }
        }

        errors.forEach(System.out::println);
    }
}
