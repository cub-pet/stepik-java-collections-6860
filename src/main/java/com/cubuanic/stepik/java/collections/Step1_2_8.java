package com.cubuanic.stepik.java.collections;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class Step1_2_8 {
    /**
     * Write a program that implements a simple load balancer.
     *
     * The program must read tasks from the standard input and
     * distribute them between two queues. Tasks will be processed
     * by a system (in future). Each task has a unique identifier
     * and a number indicating the load on the system (in parrots).
     *
     * The balancer should distribute tasks between queues by the
     * following rule - the task is added to the lower-load queue
     * (by the total load). If both queues have the same total load
     * indicator, the task must be added to the first queue.
     *
     * It's guaranteed, the input data contain at least two tasks.
     *
     * Input data format
     *
     * The first line contains the number of tasks. Other lines
     * consist of task descriptions - an identifier and a load
     * indicator (separated by a space).
     *
     * Output data form
     *
     * The first line should contain identifiers of tasks in the
     * first queue, the second line - in the second queue.
     *
     * Sample Input:
     * 6
     * 1 1
     * 2 1
     * 3 1
     * 4 3
     * 5 1
     * 6 1
     *
     * Sample Output:
     * 1 3 5 6
     * 2 4
     */
    public static void main(String[] args) {
        Deque<Integer> queue1 = new ArrayDeque<>();
        Deque<Integer> queue2 = new ArrayDeque<>();

        Scanner scanner = new Scanner(System.in);
        int jobCount = scanner.nextInt();
        int load1 = 0, load2 = 0;

        while (jobCount > 0) {
            int task = scanner.nextInt();
            int load = scanner.nextInt();
            if (load1 <= load2) {
                queue1.add(task);
                load1 += load;
            } else {
                queue2.add(task);
                load2 += load;
            }
            jobCount--;
        }

        while (!queue1.isEmpty()) {
            System.out.print(queue1.pop());
            if (!queue1.isEmpty()) System.out.print(" ");
        }
        System.out.println();
        while (!queue2.isEmpty()) {
            System.out.print(queue2.pop());
            if (!queue2.isEmpty()) System.out.print(" ");
        }
        System.out.println();

//        System.out.println(queue1.stream().map(Object::toString).collect(joining(" ")));
//        System.out.println(queue2.stream().map(Object::toString).collect(joining(" ")));
    }
}
