package com.cubuanic.stepik.java.collections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;

public class Step1_2_5 {
    /**
     * Write a program that reads the input elements and outputs them in the reverse order.
     * The first string contains the number of elements.
     * Each line followed the first one contains an element.
     *
     * Sample Input:
     * 3
     * 1
     * 2
     * 3
     *
     * Sample Output:
     * 3
     * 2
     * 1
     */
    public static void main(String[] args) throws IOException {
        Deque<String> stack = new ArrayDeque<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        final String countStr = reader.readLine();
        int count = Integer.parseInt(countStr);
        while (count-- > 0) {
            stack.push(reader.readLine());
        }

        while (!stack.isEmpty()) {
            System.out.println(stack.pop());
        }
    }
}
