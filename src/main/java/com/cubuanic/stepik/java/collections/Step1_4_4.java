package com.cubuanic.stepik.java.collections;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Step1_4_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Map<String, Integer> result = new TreeMap<>();

        while (scanner.hasNext()) {
            final String word = scanner.next().toLowerCase();
            int count = 1;
            if (result.containsKey(word)) {
                count = result.get(word) + 1;
            }
            result.put(word, count);
        }

        result.forEach((k, v) -> System.out.println(k + " " + v));
    }
}
