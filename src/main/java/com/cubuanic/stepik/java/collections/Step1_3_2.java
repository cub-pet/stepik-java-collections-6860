package com.cubuanic.stepik.java.collections;

import java.util.TreeSet;

public class Step1_3_2 {
    public static void main(String[] args) {
        TreeSet<String> set = new TreeSet<>();

        set.add("Gamma");
        set.add("Alpha");
        set.add("Omega");

        System.out.println(set);
    }
}
