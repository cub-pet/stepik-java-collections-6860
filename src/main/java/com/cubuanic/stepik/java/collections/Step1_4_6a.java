package com.cubuanic.stepik.java.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Step1_4_6a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // parse chars
        char[] alpha = scanner.nextLine().toCharArray();
        char[] cipher = scanner.nextLine().toCharArray();

        // build cipher mappings
        Map<Character, Character> mapping = new HashMap<>(alpha.length);
        Map<Character, Character> invertedMapping = new HashMap<>(alpha.length);
        for (int i = 0; i < alpha.length; i++) {
            mapping.put(alpha[i], cipher[i]);
            invertedMapping.put(cipher[i], alpha[i]);
        }

        System.out.println(encodeByMapping(scanner.nextLine().toCharArray(), mapping));
        System.out.println(encodeByMapping(scanner.nextLine().toCharArray(), invertedMapping));
    }

    private static String encodeByMapping(final char[] chars, Map<Character, Character> mapping) {
        return IntStream.range(0, chars.length)
            .mapToObj(i -> chars[i])
            .map(c -> mapping.get(c).toString())
            .collect(Collectors.joining());
    }
}
