package com.cubuanic.stepik.java.collections;

import java.util.ArrayList;
import java.util.Scanner;

public class Step1_1_8 {

    /**
     * Write a program that reads the list of integer number separated
     * by spaces from the standard input and then remove all numbers
     * with even indexes (0, 2, 4, and so on). After, the program should
     * output the result sequence in the reverse order.
     *
     * Sample Input: 1 2 3 4 5 6 7
     * Sample Output: 6 4 2
     */
    public static void main(String[] args) {

        // at this step of course, the Collections utility class was
        // not explored yet, so Collections.reverse() is not used

        Scanner scanner = new Scanner(System.in);
        int index = 0;
        ArrayList<String> arr = new ArrayList<>();
        while (scanner.hasNext()) {
            final String nextToken = scanner.next();
            if (index != 0) {
                arr.add(nextToken);
            }
            index = 1 - index;
        }

        StringBuilder result = new StringBuilder("");
        while (!arr.isEmpty()) {
            result.append(arr.remove(arr.size()-1));
            if (!arr.isEmpty()) {
                result.append(" ");
            }
        }

        System.out.println(result);
    }
}
