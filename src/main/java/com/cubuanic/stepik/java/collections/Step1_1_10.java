package com.cubuanic.stepik.java.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Step1_1_10 {
    /**
     * Given a sequence of strings separated by spaces.
     * Read the sequence from the standard input and store all strings to the list.
     * Output the list to the standard output using System.out.println(yourList).
     * The order of elements must be the same as in the input.
     *
     * Sample Input: Google Oracle JetBrains
     * Sample Output: [Google, Oracle, JetBrains]
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> list = new ArrayList<>();
        while (scanner.hasNext()) {
            list.add(scanner.next());
        }

        System.out.println(list);
    }
}
