package com.cubuanic.stepik.java.collections;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Step1_4_6b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] alpha = scanner.nextLine().split("");
        String[] cipher = scanner.nextLine().split("");

        Map<String, String> mapping = new HashMap<>(alpha.length);
        Map<String, String> invertedMapping = new HashMap<>(alpha.length);
        for (int i = 0; i < alpha.length; i++) {
            mapping.put(alpha[i], cipher[i]);
            invertedMapping.put(cipher[i], alpha[i]);
        }

        System.out.println(encodeByMapping(scanner.nextLine().split(""), mapping));
        System.out.println(encodeByMapping(scanner.nextLine().split(""), invertedMapping));
    }

    private static String encodeByMapping(String[] chars, Map<String, String> mapping) {
        return Arrays.stream(chars).map(mapping::get).collect(Collectors.joining());
    }
}
