package com.cubuanic.stepik.java.collections;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Step1_3_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        Set<String> result = new TreeSet<>();

        while (scanner.hasNext() && count-- > 0) {
            result.add(scanner.next());
        }

        result.forEach(System.out::println);
    }
}
